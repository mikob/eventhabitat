#define USE_VISUALIZER false
#include <iostream>
#include "habitat.h"
using namespace EventHabitat;


Habitat habitat;

// User defined conditions below
bool warm()
{
    return habitat.value("temp_1") > 75;
}

bool veryWarm()
{
    return habitat.value("temp_1") > 78;
}

bool needsVentilation()
{
    return habitat.max("temp_1", 5) > 81;
}

bool dry()
{
    return habitat.average("moisture_1", 5) < 0.2;
}

bool wet() 
{
    return habitat.value("moisture_1") > 0.5;
}

bool hotAndDry()
{
    return (habitat.average("moisture_1", 5) < 0.3 &&
            habitat.value("temp_1") > 82);
}

bool closedWindowTemps() 
{
    return habitat.value("temp_1") < 75;
}

// User-defined main habitat function
int startHabitat()
{
    habitat.addSensor(QStringList() << "temp_1" << "moisture_1");

    // Last argument: 1 is a low priority, higher numbers are higher priority
    habitat.notify("fan1", warm, "power,30", 1);
    habitat.notify(QStringList() << "fan1" << "fan2", veryWarm, "power,70", 2);
    habitat.notify(QStringList() << "window1" << "window2" << "window3",
            needsVentilation, "on", 2);
    habitat.notify("sprinkler1", dry, "on", 5);
    habitat.notify("sprinkler1", wet, "off", 5);
    habitat.notify("sprinkler1", hotAndDry, "on", 5);
    habitat.notify(QStringList() << "fan1" << "fan2" << "fan3", 
            hotAndDry, "power,100", 5);
    habitat.notify(QStringList() << "window1" << "window2" << "window3", 
            hotAndDry, "on", 5);
    habitat.notify(QStringList() << "window1" << "window2" << "window3", 
            closedWindowTemps, "off", 3);
    habitat.notify(QStringList() << "fan1" << "fan2" << "fan3", 
            closedWindowTemps, "power,0");

    // Only the notifications with a greater priority than the threshold will
    // be sent.
    habitat.setNotifyThreshold(0);

    habitat.start();  
}

int main(int argc, char** argv)
{
    startHabitat();
}
