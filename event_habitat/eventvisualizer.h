#ifndef EVENTVISUALIZER_H
#define EVENTVISUALIZER_H

#include <QMainWindow>
#include <QThread>
//#include "habitat.h"
#include <QTreeWidgetItem>
#include "structures.h"

#include <qwt_plot_curve.h>

namespace Ui {
class EventVisualizer;
}

class EventVisualizer : public QMainWindow
{
    Q_OBJECT
public slots:
    void Update();

    void sensorSelection(QTreeWidgetItem*,int);

signals:
    
public:
    explicit EventVisualizer(QWidget *parent = 0);
    ~EventVisualizer();

    int Add_Sensors(QStringList * sensorNames, QHash<QString, sensorStruct> * sensorlist);
    int Add_Devices(QStringList * deviceNames, QHash<QString, QHash<QString, bool> > * devicelist);
    //int Add_Rules();


    
private:
    Ui::EventVisualizer *ui;

    QStringList *sensorNames;
    QHash<QString, sensorStruct> * sensors;

    QStringList *deviceNames;
    QHash<QString, QHash<QString, bool> > *devices;

    QVector<ruleStruct> *rules;

    QwtPlotCurve *curve1;
};



#endif // EVENTVISUALIZER_H
