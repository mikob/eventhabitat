#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <QString>
#include <QStringList>
#include <iostream>
#include <string>
#include <limits>
#include <QVector>
#include <QDateTime>
#include <QHash>

typedef bool (*conditionFn)();

struct sensorEntry  //a sensor data point.
{
    double value;
    QDateTime time;  //when this value was taken
};

struct sensorStruct  //a sensor
{
    QVector<sensorEntry> data;  //each sensor has a buffer of data

    //min and max added by joey
    double min;//only used by event visualizer. other use not guaranteed
    double max;//only used by the event visualizer. other use not guaranteed
    double average;
    int maxSamples;  //the max size of the buffer
};

struct ruleStruct  //stores what user defines in each notify() call
{
    int priority;  //the priority of the above message
    bool isActive;

    QStringList devices;  //all of the devices the user wants to notify with this condition
    conditionFn condition;  //a function the user defines in main.  if this function returns true, all devices will output the ruleMessage
    QString ruleMessage;    //the message the user wants to output to all devices once the condition is met
};

struct deviceEntry  //a sensor data point.
{
    bool active;

    QDateTime time;  //when this status was set
};

#endif // STRUCTURES_H
