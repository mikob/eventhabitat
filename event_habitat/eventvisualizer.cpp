#include "eventvisualizer.h"
#include "ui_eventvisualizer.h"


EventVisualizer::EventVisualizer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EventVisualizer)
{
    ui->setupUi(this);

    connect(this->ui->SensorTree, SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(sensorSelection(QTreeWidgetItem*,int)));
    curve1 = new QwtPlotCurve("Curve1");
    curve1->attach(this->ui->CurrentSensorPlot);
}

EventVisualizer::~EventVisualizer()
{
    delete ui;
}

void EventVisualizer::sensorSelection(QTreeWidgetItem* highlighted,int index)
{
    QHash<QString, sensorStruct> s = *sensors;
    sensorStruct selected = s[highlighted->text(0)];
    if (selected.data.size() >= 100)
    {
        this->ui->CurrentSensorPlot->setAxisScale(this->ui->CurrentSensorPlot->xBottom, selected.data.size() - 100, selected.data.size(), 10);
        this->ui->CurrentSensorPlot->setAxisScale(this->ui->CurrentSensorPlot->yLeft, selected.min, selected.max, selected.max - selected.min / 10);
        double x[100];
        double y[100];

        curve1->setSamples(&x[0], &y[0], 100);
    }
    else
    {
        this->ui->CurrentSensorPlot->setAxisScale(this->ui->CurrentSensorPlot->xBottom, 0, selected.data.size(), selected.data.size() / 10);
        this->ui->CurrentSensorPlot->setAxisScale(this->ui->CurrentSensorPlot->yLeft, selected.min, selected.max, selected.max - selected.min / 10);
        double x[selected.data.size()];
        double y[selected.data.size()];

        curve1->setSamples(&x[0], &y[0], selected.data.size());
    }
    this->ui->CurrentSensorPlot->replot();
    this->ui->CurrentSensorPlot->setTitle(highlighted->text(0));
}

/*The Big Guy Function*/
void EventVisualizer::Update()
{
    //initializers


    //Necessary computations
    qDebug() << "Size of Sensor list: " + sensorNames->size();


    //empty all of the display widgets
    this->ui->SensorTree->clear();
    this->ui->DeviceTree->clear();

    //rebuild the display widgets
    QStringList::iterator sit;
    for (sit = sensorNames->begin(); sit < sensorNames->end(); sit++)
    {
        QString cur = *sit;

        QTreeWidgetItem * toadd = new QTreeWidgetItem;

        //add all displays for the sensor class here
        toadd->setText(0, cur);
        QHash<QString, sensorStruct> s = *sensors;
        toadd->setText(1, QString().setNum(s[cur].data.back().value));
        toadd->setText(2, QString().setNum(s[cur].average));
        toadd->setText(3, QString().setNum(s[cur].max));
        toadd->setText(4, QString().setNum(s[cur].min));

        //throw in relations here, once we establish the class

        this->ui->SensorTree->addTopLevelItem(toadd);
    }
    for (sit = deviceNames->begin(); sit < deviceNames->end(); sit++)
    {
        QString cur = *sit;

        QTreeWidgetItem * toadd = new QTreeWidgetItem;

        toadd->setText(0, cur);
        //toadd->setText(1, ); I HAVE NO WAY TO SEE IF THE SENSOR IS LAST SET TO ACTIVE OR INACTIVE nor when

        this->ui->DeviceTree->addTopLevelItem(toadd);
    }
}

int EventVisualizer::Add_Sensors(QStringList * names, QHash <QString, sensorStruct> * sensorlist)
{
    if (names->empty() || sensorlist->empty())
        return 2;
    else if (names->size() != sensorlist->size())
        return 1;
    else
    {
        sensorNames = names;
        sensors = sensorlist;
        return 0;
    }
}
int EventVisualizer::Add_Devices(QStringList * names, QHash<QString, QHash<QString, bool> > * devicelist)
{
    if (names->empty() || devicelist->empty())
        return 2;
    else if (names->size() != devicelist->size())
        return 1;
    else
    {
        deviceNames = names;
        devices = devicelist;
        return 0;
    }
}



