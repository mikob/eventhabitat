#ifndef HABITAT_H
#define HABITAT_H

#define USE_VISUALIZER false

#include "structures.h"

#if USE_VISUALIZER 
#include "eventvisualizer.h"
#endif

#include <QObject>
#include <QString>
#include <QStringList>
#include <iostream>
#include <string>
#include <limits>
#include <QVector>
#include <QDateTime>
#include <QHash>
#include <QLocalSocket>

namespace EventHabitat
{
    class Habitat : public QObject
    {
        Q_OBJECT

    public:
        int start();

        void setNotifyThreshold(int notifyThreshold);
        void notify(QString deviceName, bool conditionFunction(), QString message, int priority = 1);
        void notify(QStringList deviceNames, bool conditionFunction(), QString message, int priority = 1);
        void addSensor(QString sensorName, int bufferSize = 1000);
        void addSensor(QStringList sensorNames, int bufferSize = 1000);

        // User helper functions for getting sensor values
        double value(QString sensorName);
        double average(QString sensorName, int maxSamples = -1);
        double max(QString sensorName, int maxSamples = -1);
        double min(QString sensorName, int maxSamples = -1);
        double velocity(QString sensorName, int maxSamples = -1);

    private:
        static const QString INPUT_SOCKET_LOC;
        static const QString OUTPUT_SOCKET_LOC;
        QLocalSocket* inputSocket;
        QLocalSocket* outputSocket;
        
        // < sensorName, < dataVector<value, time>, maxBufferSize >
        QHash<QString, sensorStruct> m_sensors;

        //all the rules that the user defines in all of this notify() calls
        QVector<ruleStruct> m_rules;  

        int m_notifyThreshold;
        bool m_criticalErrorFound;

        //added by joey
        QStringList sensorNames;
        QStringList m_devices;   // All the device names
        void addDevice(QStringList devices);
        void addDevice(QString device);

        void checkRules();

    public slots:
        void socketConnected();
        void socketDisconnected();
        void storeSensorData();
        void displaySocketError(QLocalSocket::LocalSocketError socketError);
    };
}

#endif // HABITAT_H
