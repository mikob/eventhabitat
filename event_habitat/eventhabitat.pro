
QT += network
TEMPLATE = app
TARGET =

# Input
HEADERS += habitat.h structures.h \
    ui_eventvisualizer.h \
    eventvisualizer.h \
    ui_eventvisualizer.h \
    eventvisualizer.h
SOURCES += habitat.cpp main.cpp \
    eventvisualizer.cpp

FORMS += \
    eventvisualizer.ui

#the following will change for each machine
unix:!macx:!symbian: LIBS += -L$$PWD/lib/x32/ -lqwt

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include
