#include <QApplication>
#include <QSignalMapper>
#include <QStringBuilder>
#include "habitat.h"
using namespace EventHabitat;

const QString Habitat::INPUT_SOCKET_LOC = "/tmp/habitat_in";
const QString Habitat::OUTPUT_SOCKET_LOC = "/tmp/habitat_out";

int m_notifyThreshold = 0;
bool m_criticalErrorFound = false;

//add a single sensor to the system, each sensor with its own buffersize
void Habitat::addSensor(QString sensor, int bufferSize)
{
    addSensor(QStringList() << sensor, bufferSize);
}

//add multiple sensors to the system, all with the same data buffer size
void Habitat::addSensor(QStringList sensors, int bufferSize)
{
    sensorStruct s;
    s.maxSamples = bufferSize;

    foreach (QString sensorName, sensors)
    {
        m_sensors.insert(sensorName,s);

        //added by joey
        sensorNames.append(sensorName);
    }
}

//add a single device to the system
void Habitat::addDevice(QString device)
{
    addDevice(QStringList() << device);
}

//add multiple devices to the system
void Habitat::addDevice(QStringList devices)
{
    foreach (QString deviceName, devices)
    {
        m_devices.append(deviceName);
    }
}

//adds a user-defined function to the system.  The system continuously checks this function, and if the function returns true,
//the system will output a message (argument 3) at a user-defined priority (argument 4) to the device (argument 1)
//The user defines the function in argument 2 in the main file.
void Habitat::notify(QString deviceName, bool conditionFunction(), QString message, int priority)
{
    notify(QStringList() << deviceName, conditionFunction, message, priority);
}

//adds a user-defined function to the system.  The system continuously check this functions, and if the function returns true,
//the system will output a message (argument 3) at a user-defined priority (argument 4) to all the devices listed in argument 1
//The user defines the function in argument 2 in the main file.
void Habitat::notify(QStringList deviceNames, bool conditionFunction(), QString message, int priority)
{
    //Error checking sensor names in user-defined condition function
    conditionFunction();
    if (m_criticalErrorFound) return;

    ruleStruct r;
    foreach(QString deviceName, deviceNames)
    {
        if (!m_devices.contains(deviceName))  //error-checking device names
        {
            std::cout << "New device detected: " << deviceName.toStdString() << std::endl;
            m_devices << deviceName;
        }

        r.devices << deviceName;
    }

    r.condition = conditionFunction;
    r.ruleMessage = message;
    r.priority = priority;
    r.isActive = false;

    m_rules.push_back(r);
}

//call this to check all the user-defined condition functions.  If any function returns true, output that function's user-defined message.
//If the user sets a notifyThreshold, then only the rules defined with a priority >= m_notifyThreshold will output a message
void Habitat::checkRules()
{
    //foreach (ruleStruct r, m_rules)
    for (int i = 0; i < m_rules.size(); i++)
    {
        ruleStruct r = m_rules.value(i);
        if (r.priority < m_notifyThreshold) continue;

        if ((r.condition)() == true)
        {
            // check if the message has already been outputted.  If not, output it
            if (r.isActive == false)  
            {
                m_rules[i].isActive = true;
                QString devicesCommaDelimited = r.devices.join(",");
                QString msg = devicesCommaDelimited % "\t" % r.ruleMessage % "$";

                qDebug() << "Notification: " <<  msg;

                outputSocket->write(msg.toLocal8Bit().data());
            }
        }
        else
        {
            m_rules[i].isActive = false;
        }
    }
}

//the user can call this to set a priority threshold.  Only rules defined with a priority greater than or equal to this threshold
//will output messages
void Habitat::setNotifyThreshold(int notifyThreshold)
{
    m_notifyThreshold = notifyThreshold;
}

//get the current value of a sensor (the last value read from the sensor)
double Habitat::value(QString sensorName)
{
    if (!m_sensors.contains(sensorName)) //error-check to see if the user defined the sensor first
    {
        std::cout << "ERROR: Sensor " << sensorName.toStdString() << " has not been added to the system.\n  Please add this sensor before using it." << std::endl;
        m_criticalErrorFound = true;
        return 0.0;
    }

    if (m_sensors[sensorName].data.size() == 0)  //ensure there is at least one value
        return 0.0;

    return m_sensors[sensorName].data.last().value;
}

//get the average of all the values sampled from the sensor
//if the user defines argument 2, then this returns the average of only the past X number of samples
double Habitat::average(QString sensorName, int maxSamples)
{
    if (!m_sensors.contains(sensorName))  //error-check to see if the user defined the sensor first
    {
        std::cout << "ERROR: Sensor " << sensorName.toStdString() << " has not been added to the system.\n  Please add this sensor before using it." << std::endl;
        m_criticalErrorFound = true;
        return 0.0;
    }

    int numSamples = 0;
    double total = 0;

    for (int i = m_sensors[sensorName].data.size() - 1; i >= 0; i--)  //iterate backwards through this sensor's data
    {
        total += m_sensors[sensorName].data.at(i).value;
        numSamples++;
        if (numSamples == maxSamples) break;
    }

    return numSamples == 0 ? 0.0 : total / numSamples;
}

//get the maximum from all the values sampled from the sensor
//if the user defines argument 2, then this returns the maximum from only the past X number of samples
double Habitat::max(QString sensorName, int maxSamples)
{
    if (!m_sensors.contains(sensorName))  //error-check to see if the user defined the sensor first
    {
        std::cout << "ERROR: Sensor " << sensorName.toStdString() << " has not been added to the system.\n  Please add this sensor before using it." << std::endl;
        m_criticalErrorFound = true;
        return 0.0;
    }

    double maximum = -1 * std::numeric_limits<double>::max();
    double currentValue = 0.0;
    int numSamples = 0;

    for (int i = m_sensors[sensorName].data.size() - 1; i >= 0; i--)  //iterate backwards through this sensor's data
    {
        currentValue = m_sensors[sensorName].data.at(i).value;
        if (currentValue > maximum)
            maximum = currentValue;

        numSamples++;
        if (numSamples == maxSamples) break;
    }

    //added by joey
    m_sensors[sensorName].max = maximum;

    return maximum;
}

//get the minimum from all the values sampled from the sensor
//if the user defines argument 2, then this returns the minimum from only the past X number of samples
double Habitat::min(QString sensorName, int maxSamples)
{
    if (!m_sensors.contains(sensorName))  //error-check to see if the user defined the sensor first
    {
        std::cout << "ERROR: Sensor " << sensorName.toStdString() << " has not been added to the system.\n  Please add this sensor before using it." << std::endl;
        m_criticalErrorFound = true;
        return 0.0;
    }

    double minimum = std::numeric_limits<double>::max();
    double currentValue = 0.0;
    int numSamples = 0;

    for (int i = m_sensors[sensorName].data.size() - 1; i >= 0; i--)  //iterate backwards through this sensor's data
    {
        currentValue = m_sensors[sensorName].data.at(i).value;
        if (currentValue < minimum)
            minimum = currentValue;

        numSamples++;
        if (numSamples == maxSamples) break;
    }

    //added by joey
    m_sensors[sensorName].min = minimum;

    return minimum;
}

//get the average velocity between the last two data points sampled from a sensor
//if the user defines argument 2, then this returns the average velocity between the current value and a value that was taken
//X number of samples in the past
double Habitat::velocity(QString sensorName, int maxSamples)
{
    if (!m_sensors.contains(sensorName))  //error-check to see if the user defined the sensor first
    {
        std::cout << "ERROR: Sensor " << sensorName.toStdString() << " has not been added to the system.\n  Please add this sensor before using it." << std::endl;
        m_criticalErrorFound = true;
        return 0.0;
    }

    int currentNumSamples = m_sensors[sensorName].data.size();
    if (currentNumSamples < 2)  //need at least 2 data points to compute velocity
        return 0.0;

    if (maxSamples < 1)
        maxSamples = 1;

    //take the earliest sample possible if maxSamples is bigger than how many samples we currently have
    if (maxSamples >= currentNumSamples)
        maxSamples = currentNumSamples - 1;

    double initialValue = m_sensors[sensorName].data.at((currentNumSamples - 1) - maxSamples).value;
    QDateTime initialTime =  m_sensors[sensorName].data.at((currentNumSamples - 1) - maxSamples).time;
    double finalValue = m_sensors[sensorName].data.at(m_sensors[sensorName].data.size() - 1).value;
    QDateTime finalTime = m_sensors[sensorName].data.at(m_sensors[sensorName].data.size() - 1).time;

    int timeDifference = initialTime.secsTo(finalTime);
    if (timeDifference < 1)
        timeDifference = 1;

    return (finalValue - initialValue) / timeDifference;
}

// Slot
void Habitat::storeSensorData()
{
    // MIKO: do m_sensors["sensor_name"].maxSamples to find out the max buffer size for sensor.
    // call m_sensors["sensor_name"].data.size to find out the current buffer size.
    // if the size is exceeded, call m_sensors["sensor_name"].data.pop_front()
    // and please replace all the following code with you getting one of each sensor's value from the input socket

    char buf[1024];
    inputSocket->readLine(buf, sizeof(buf));

    sensorEntry s;
    QString device;
    QString value;
    QString data = QString(buf);

    QStringList separatedData = data.split('\t');
    device = separatedData[0];
    value = separatedData[1];
    s.value = value.toDouble();
    s.time = QDateTime::currentDateTime();
    

    qDebug() << "Device: " << device << ", value: " << s.value;

    // Make sure we don't outgrow the buffer
    if (m_sensors[device].data.size() >= m_sensors[device].maxSamples)
    {
        m_sensors[device].data.pop_front();
    }

    m_sensors[device].data.push_back(s);

    checkRules();
}

void Habitat::socketConnected()
{
    bool isInput = false;
    QLocalSocket* socket = (QLocalSocket*)this->sender();

    if (socket->fullServerName() == INPUT_SOCKET_LOC)
    {
        isInput = true;
    }

    QString socketType = isInput ? "input" : "output";
    qDebug() << "Successfully connect to " << socketType << " socket." << endl;
}

void Habitat::socketDisconnected()
{
    bool isInput = false;
    QLocalSocket* socket = (QLocalSocket*)this->sender();

    if (socket->fullServerName() == INPUT_SOCKET_LOC)
    {
        isInput = true;
    }

    QString socketType = isInput ? "input" : "output";
    qDebug() << "Disconnected from " << socketType << " socket." << endl;
}

void Habitat::displaySocketError(QLocalSocket::LocalSocketError socketError)
{
    bool isInput = false;
    QLocalSocket* socket = (QLocalSocket*)this->sender();

    if (socket->fullServerName() == INPUT_SOCKET_LOC)
    {
        isInput = true;
    }

    if (socket->fullServerName() == "")
    {
        qDebug() << "Input or output socket error.";
    } else if (isInput) {
        qDebug() << "Input socket error:";
    } else {
        qDebug() << "Ouput socket error:";
    }

    switch (socketError) {
        case QLocalSocket::ConnectionRefusedError:
            qDebug() << "Connection refused error.";
            break;
        case QLocalSocket::PeerClosedError:
            qDebug() << "Peer closed error.";
            break;
        case QLocalSocket::ServerNotFoundError:
            qDebug() << "Connection Server not found error.";
            break;
        case QLocalSocket::SocketAccessError:
            qDebug() << "Socket access error.";
            break;
        case QLocalSocket::SocketResourceError:
            qDebug() << "Socket resource error.";
            break;
        case QLocalSocket::SocketTimeoutError:
            qDebug() << "Socket timeout error.";
            break;
        case QLocalSocket::DatagramTooLargeError:
            qDebug() << "Datagram too large error.";
            break;
        case QLocalSocket::ConnectionError:
            qDebug() << "Connection error.";
            break;
        case QLocalSocket::UnsupportedSocketOperationError:
            qDebug() << "Unsupported socket operation error.";
            break;
        case QLocalSocket::UnknownSocketError:
            qDebug() << "Unknown socket error.";
            break;
        default:
            if (isInput) {
                qDebug() << "Error: " << inputSocket->errorString();
            } else {
                qDebug() << "Error: " << outputSocket->errorString();
            }
            break;
    }
}

// Summary:
//  Check the rules anytime new sensor data comes in.
//  This is what the user runs to start the system.
int Habitat::start()
{

    // Dumby variables for QCoreApplication
    int argc = 1;
    char *argv[] = {"", NULL};

    QApplication a(argc, argv);

    inputSocket = new QLocalSocket(this);
    outputSocket = new QLocalSocket(this);

    QObject::connect(inputSocket, SIGNAL(connected()), this, SLOT(socketConnected()));
    QObject::connect(inputSocket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
    QObject::connect(inputSocket, SIGNAL(readyRead()), this, SLOT(storeSensorData()));
    QObject::connect(inputSocket, SIGNAL(error(QLocalSocket::LocalSocketError)),
            this, SLOT(displaySocketError(QLocalSocket::LocalSocketError)));

    QObject::connect(outputSocket, SIGNAL(connected()), this, SLOT(socketConnected()));
    QObject::connect(outputSocket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
    QObject::connect(outputSocket, SIGNAL(error(QLocalSocket::LocalSocketError)),
            this, SLOT(displaySocketError(QLocalSocket::LocalSocketError)));

    inputSocket->connectToServer(INPUT_SOCKET_LOC, QIODevice::ReadOnly);
    outputSocket->connectToServer(OUTPUT_SOCKET_LOC, QIODevice::WriteOnly);

#if EVENT_VISUALIZER
    EventVisualizer w;
    qDebug() << "using visualizer";

    w.Add_Sensors(&sensorNames, &m_sensors);
    // w.Add_Devices(&deviceNames, &m_devices);
    qDebug() << sensorNames;
    w.Update();

    w.show();
#endif

    // this needs to be a signal
    // if (m_criticalErrorFound) return;
    qDebug() << "Awaiting input from socket.";

    return a.exec();
}

