var app = require('http').createServer(handler),
    io = require('socket.io').listen(app),
    fs = require('fs'),
    path = require('path'),
    net = require('net');

app.listen(80);

function handler(request, response) {
    var filePath = '.' + request.url;
    console.log('filepath: ' + filePath);

    if (filePath == './') {
        filePath = './greenhouse.html';
    }

    path.exists(filePath, function(exists) {
        console.log('Path exists? ' + exists);
        if (exists)
        {
            fs.readFile(path.join(__dirname, filePath),
                function (err, data) {
                    if (err) {
                        response.writeHead(500);
                        response.end('Error loading greenhouse.html');
                    } else {
                        response.writeHead(200);
                        response.end(data);
                    }
                });
        } else {
            response.writeHead(404);
            response.end();
        }
    });
}


io.sockets.on('connection', function(webSocket) {
    console.log('Websocket connected');

    // Server within a server?
    net.createServer(function(unixSocket) {
        console.log('Created unix socket server');
        unixSocket.setEncoding('ascii');
        unixSocket.on('data', function(data) {
            console.log('data: ' + data);
            webSocket.emit('news', data);
        });

    }).listen('/tmp/habitat_out');

});
