var  FAN_OFFSET_X = 37
    ,FAN_OFFSET_Y = 10
    ,MS_PER_FRAME = 17
    ,CANVAS_WIDTH = 800
    ,CANVAS_HEIGHT = 600
    ,SPRINKLER1_X = 123
    ,SPRINKLER1_Y = 100
    ,SPRINKLER2_X = 400
    ,SPRINKLER2_Y = 134
    ,SPRINKLER3_X = 600
    ,SPRINKLER3_Y = 134
    ,WATER_DROPLET_DENSITY = 2;

window.onload = function() {
    var socket = io.connect();

    socket.on('news', function(rawNews) {
        var rawData = rawNews.split('$');

        for (var i = 0; i < rawData.length - 1; i++)
        {
            console.log('News: ' + rawData[i]);
            var data = rawData[i].split('\t');
            var devices = data[0].split(',');
            var msg = data[1];

            routeNews(devices, msg);
        }
    });

    var deviceCtrlNews = {};
    var scene = sjs.Scene({w: CANVAS_WIDTH, h: CANVAS_HEIGHT, autoPause: false });
    scene.loadImages(['GreenhouseBG.png', 'ClosedWindow.png',
            'OpenWindow.png', 'Fan.png'], function() {

        var droplets = [];
        // About 60 fps
        var ticker = scene.Ticker(paint, { tickDuration: MS_PER_FRAME});

        var background = scene.Layer('background', {
            useCanvas: true,
            autoClear: false
        });

        var foreground = scene.Layer('foreground', {
            useCanvas: true,
            autoClear: true
        });

        var particleground = scene.Layer('particleground', {
            useCanvas: true,
            autoClear: true
        });

        var partCanvas = document.getElementById('sjs0-particleground');
        var ctx = partCanvas.getContext('2d');
        var droplet = function(x, y, dx, dy, rotate) {
            return {
                MAX_AGE: 60, // in frames
                dead: false,
                age: 0,
                x: x,
                y: y,
                dx: dx,
                dy: dy,
                dy2: 0.05,

                update: function() {
                    this.age++;
                    if (this.age > this.MAX_AGE) {
                        // die
                        this.dead = true;
                    } else {
                        // update params
                        if (rotate) {
                            this.dx += this.dy2;
                        } else {
                            this.dy += this.dy2;
                        }
                        this.x += this.dx;
                        this.y += this.dy;
                        //
                        ctx.fillStyle = "rgba(108,108,255," + 
                            (this.MAX_AGE - this.age)/(this.MAX_AGE) + ")";
                        ctx.beginPath();
                        ctx.arc(this.x, this.y, 1.5, 0, Math.PI * 2, true);
                        ctx.closePath();
                        ctx.fill();
                    }
                }
            }
        };

        var bgSprite = background.Sprite('GreenhouseBG.png');
        bgSprite.update();

        // Scene elements
        var fan1 = foreground.Sprite('Fan.png');
        fan1.isFan = true;
        var fan2 = foreground.Sprite('Fan.png');
        fan2.isFan = true;
        var fan3 = foreground.Sprite('Fan.png');
        fan3.isFan = true;
        var window1 = foreground.Sprite('ClosedWindow.png');
        window1.isWindow = true;
        var window2 = foreground.Sprite('ClosedWindow.png');
        window1.isWindow = true;
        var window3 = foreground.Sprite('ClosedWindow.png');
        window3.isWindow = true;

        sjs.Sprite.prototype.on = function() {
            if (this.isWindow === true)
            {
                this.loadImg('OpenWindow.png');
            }
        };

        sjs.Sprite.prototype.off = function() {
            if (this.isWindow === true)
            {
                this.loadImg('ClosedWindow.png');
            }
        };

        sjs.Sprite.prototype.power = function(power) {
            if (this.isFan === true)
            {
                this.rotateSpeed = power * -0.002;
            }
        };

        // A wrapper function for the Sprite JS update function
        sjs.Sprite.prototype.updateDevice = function() {
            if (this.rotateSpeed)
            {
                this.rotate(this.rotateSpeed);
            }
            this.update();
        };

        window1.position(330, 340);
        window2.position(495, 340);
        window3.position(660, 340);
        window1.setOpacity(0.84);
        window2.setOpacity(0.84);
        window3.setOpacity(0.84);

        fan1.position(window1.x + FAN_OFFSET_X, window1.y + FAN_OFFSET_Y);
        fan2.position(window2.x + FAN_OFFSET_X, window2.y + FAN_OFFSET_Y);
        fan3.position(window3.x + FAN_OFFSET_X, window3.y + FAN_OFFSET_Y);

        var sprinklerOn = false;
        function paint()
        {
            for (device in deviceCtrlNews)
            {
                if (device === 'sprinkler1')
                {
                    if (deviceCtrlNews[device].on === true)
                    {
                        sprinklerOn = true;
                    } else {
                        sprinklerOn = false;
                    }
                }
                else if (deviceCtrlNews[device].on === true)
                {
                    eval(device + ".on()");
                }
                else if (deviceCtrlNews[device].on === false)
                {
                    eval(device + ".off()");
                }
                else if (typeof deviceCtrlNews[device].power !== 'undefined')
                {
                    eval(device + ".power(" + Number(deviceCtrlNews[device].power) +
                        ")");
                }
                delete deviceCtrlNews[device];
            }
                
            if (sprinklerOn)
            {
                // every 11 frames, 5 times per second
                // if (ticker.currentTick % (1000 / MS_PER_FRAME) / 20)
                for (var j = 0; j < WATER_DROPLET_DENSITY; j++)
                {
                    createDroplet(SPRINKLER1_X, SPRINKLER1_Y, true);
                    createDroplet(SPRINKLER2_X, SPRINKLER2_Y, false);
                    createDroplet(SPRINKLER3_X, SPRINKLER3_Y, false);
                }
            }

            function createDroplet(x, y, /*hack*/rotate)
            {
                var dx = 3 - Math.random() * 6;
                var dy = 1.5;
                if (rotate)
                {
                    dy = dx;
                    dx = 0.01;
                }
                droplets.push(droplet(x, y, dx, dy, rotate));
            }

            ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

            for (var p = 0; p < droplets.length; p++)
            {
                if (droplets[p].dead === true) {
                    var deadGuy = droplets.splice(p, 1);
                    delete deadGuy;
                } else {
                    droplets[p].update();
                }
            }

            fan1.updateDevice();
            fan2.updateDevice();
            fan3.updateDevice();
            window1.updateDevice();
            window2.updateDevice();
            window3.updateDevice();
        }

        ticker.run();
    });

    function routeNews(devices, msg)
    {
        for (i in devices)
        {
            var device = devices[i];
            var commands = msg.split(',');
            switch (commands[0])
            {
                case 'on':
                    if (typeof deviceCtrlNews[device] == 'undefined')
                    {
                        deviceCtrlNews[device] = {};
                    }
                    deviceCtrlNews[device].on = true;
                    break;
                case 'off':
                    if (typeof deviceCtrlNews[device] == 'undefined')
                    {
                        deviceCtrlNews[device] = {};
                    }
                    deviceCtrlNews[device].on = false;
                    break;
                case 'power':
                    if (typeof deviceCtrlNews[device] == 'undefined')
                    {
                        deviceCtrlNews[device] = {};
                    }
                    deviceCtrlNews[device].power = commands[1];
                    break;
            }
        }
    }
};
