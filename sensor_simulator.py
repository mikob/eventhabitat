import socket, os, time

INPUT_SOCKET_LOC = '/tmp/habitat_in'
SENSOR_DATA_FILE = open('sensor_simulation.dat')
s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
try:
    os.remove(INPUT_SOCKET_LOC)
except OSError:
    pass
s.bind(INPUT_SOCKET_LOC)
print "Simulator ready."

def waitForConnect():
    # this blocks until we get an incoming connection
    s.listen(1)  
    (conn, addr) = s.accept()
    print "Incoming connection received"
    try:
        outputData(conn)
    except socket.error:
        waitForConnect()

def outputData(conn):
    while 1:
        for line in SENSOR_DATA_FILE:
            # the data separated
            line = line.strip('\n')
            [sensor_name, value] = line.split('\t')
            conn.sendall(line)
            print "Sending " + line
            time.sleep(0.5)
        SENSOR_DATA_FILE.seek(0);

waitForConnect()
